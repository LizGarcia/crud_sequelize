"use strict";
module.exports = (sequelize, DataTypes) => {
    const car = sequelize.define(
        "car", {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true,
            },
            name: DataTypes.STRING,
            model: DataTypes.STRING,
            year: DataTypes.INTEGER,
        }, {
            tableName: "cars",
        }
    );
    return car;
};