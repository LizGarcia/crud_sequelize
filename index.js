const express = require('express');
const route = require('./routes/route');
const path = require("path");
const routes = require("./routes/route");

const app = express();

//Configuración
app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');
app.set('views', './views');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Rutas
app.use("/", route(express));

//Archivos estáticos
app.use(express.static(path.join(__dirname, 'public')));


app.listen(app.get('port'), () => {
    console.log('Server on port 3000');
});

module.exports = app;