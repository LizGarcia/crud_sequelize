const car = require("./../models");
const self = {};

//Listar
self.getAll = async(req, res) => {
    try {
        console.log('body');
        let data = await car.findAll({
            attributes: ["id", "name", "model", "year"]
        });
        return res.json({
            data: data
        });
        /*
        res.render("cars", {
            data: data,
        });
        */
    } catch (error) {
        res.status(500).json({
            status: "Error al enlistar",
            data: error
        })
    }
}

//Guardar
self.save = async(req, res) => {
    try {
        let body = req.body;
        let data = await car.create(body);
        //res.redirect("/");
        return res.json({
            status: "Guardado correctamente",
            data: data
        });
    } catch (error) {
        res.status(500).json({
            status: "Error al guardar",
            data: error
        })
    }
}

//Editar
self.update = async(req, res) => {
    try {
        let id = req.params.id;
        let body = req.body;
        console.log(body);
        let data = await car.update(body, {
            where: {
                id: id,
            }
        });
        res.render("carsEdit", {
            data: rows[0],
        });
        return res.json({
            status: "Actualizado con éxito",
            data: data
        })

    } catch (error) {
        res.status(500).json({
            status: "Error al actualizar",
            data: error
        })
    }
}

//ELIMINAR
self.delete = async(req, res) => {
    try {
        let id = req.params.id;
        let data = await car.destroy({
            where: {
                id: id
            }
        });
        res.redirect("/");

        return res.json({
            status: "Eliminado correctamente",
            data: data
        })
    } catch (error) {
        res.status(500).json({
            status: "Error al eliminar",
            data: error
        })
    }
}

//Obtener
self.get = async(req, res) => {
    try {
        let id = req.params.id;
        let data = await car.findOne({
            attributes: ["id", "name", "model", "year"],
            where: {
                id: id
            }
        });
        console.log(data);
        /*
        res.render("cars", {
            data: data,
        });
       
        return res.json({
            status: "ok",
            data: data
        })
        */
    } catch (error) {
        res.status(500).json({
            status: "error",
            data: error
        })
    }
}

module.exports = self;