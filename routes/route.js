const carController = require("./../controllers/carController");

module.exports = function(express) {
    const router = express.Router();
    // Rutas del carro
    router.get("/car", carController.getAll);
    router.post("/car", carController.save);
    router.put("/car/:id", carController.update);
    router.delete("/car/:id", carController.delete);

    return router;
};